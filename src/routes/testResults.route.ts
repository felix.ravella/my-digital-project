
import bodyParser from 'body-parser';
import { Application } from 'express';
import { deleteTestResult, getProfileTestResults, getProfilePendingTestResult, postTestResult, patchTestResult } from '../controllers/testResults.controller';
import { authenticationMiddleware } from '../middlewares/authenticationMiddleware';
import { isSameAccountMiddleware } from '../middlewares/isSameAccountMiddleware';

export function testResultsRoute(app: Application) {
  
  //app.get('/testResults/:id', getTestResult);
  app.get('/users/:id/testResults', [authenticationMiddleware, isSameAccountMiddleware], getProfileTestResults);
  app.get('/users/:id/testResults/pending', [authenticationMiddleware, isSameAccountMiddleware], getProfilePendingTestResult);
  
  app.post('/testResults', [authenticationMiddleware, bodyParser.json()], postTestResult);
  app.patch('/testResults/:id', [authenticationMiddleware, bodyParser.json()], patchTestResult);
  app.delete('/testResults/:id', [authenticationMiddleware], deleteTestResult);

}

