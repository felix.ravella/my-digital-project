import bodyParser from 'body-parser';
import { Application } from 'express';
import { deleteComment, getArticleComments, getComment, getComments, patchComment, postComment } from '../controllers/comments.controller';
import { authenticationMiddleware } from '../middlewares/authenticationMiddleware';

export function commentsRoute(app: Application) {

  app.get('/comments', getComments)
  app.get('/comments/:id', getComment)
  app.get('/articles/:id/comments', getArticleComments)

  app.post('/comments', [bodyParser.json(), authenticationMiddleware], postComment);
  app.patch('/comments/:id', bodyParser.json(), patchComment);
  app.delete('/comments/:id', bodyParser.json(), deleteComment);

}

