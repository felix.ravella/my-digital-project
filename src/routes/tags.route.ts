import { Application } from 'express';
import { getTag, getTags } from '../controllers/tags.controller';

export function tagsRoutes(app: Application) {
  app.get('/tags', getTags);

  app.get('/tags/:id', getTag);

}