import bodyParser from 'body-parser';
import { Application } from 'express';
import { deleteArticle, getArticle, getArticles, patchArticle, postArticle } from '../controllers/articles.controller';
import { authenticationMiddleware } from '../middlewares/authenticationMiddleware';
import { isAdminMiddleware } from '../middlewares/isAdminMiddleware';

export function articlesRoutes(app: Application) {
  app.get('/articles', getArticles);
  app.get('/articles/:id', getArticle);

  app.post('/articles', [bodyParser.json(), authenticationMiddleware, isAdminMiddleware], postArticle);
  app.patch('/articles/:id', [bodyParser.json(), authenticationMiddleware, isAdminMiddleware], patchArticle);
  app.delete('/articles/:id', [authenticationMiddleware, isAdminMiddleware],deleteArticle);
}