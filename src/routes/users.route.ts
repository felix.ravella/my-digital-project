import bodyParser from 'body-parser';
const jwt = require('jsonwebtoken');

import { Application } from 'express';
import { deleteUser, getUser, patchUser, postUser } from '../controllers/users.controller';
import { authenticationMiddleware } from '../middlewares/authenticationMiddleware';
import { isSameAccountMiddleware } from '../middlewares/isSameAccountMiddleware';

export function usersRoute(app: Application) {
  app.get('/users/:id', [authenticationMiddleware, isSameAccountMiddleware], getUser);

  app.post('/users', bodyParser.json(), postUser);
  app.patch('/users/:id', [bodyParser.json(), authenticationMiddleware, isSameAccountMiddleware], patchUser);
  app.delete('/users/:id', [authenticationMiddleware, isSameAccountMiddleware], deleteUser);
}