import { Application } from "express";

export function getTest(app: Application) {
  app.get('/test', (req, res) => {
    res.send('Ceci est un test!');
  });
}
