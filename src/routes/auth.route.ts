import bodyParser from 'body-parser';
import { Application } from 'express';
import { getIsAdmin, getIsLogged, postLogin } from '../controllers/auth.controller';
import { authenticationMiddleware } from '../middlewares/authenticationMiddleware';
import { isAdminMiddleware } from '../middlewares/isAdminMiddleware';

export function authRoutes(app: Application) {
  app.post('/auth/login', bodyParser.json(), postLogin);
  app.get('/auth/isLogged', authenticationMiddleware, getIsLogged);
  app.get('/auth/isAdmin', [authenticationMiddleware, isAdminMiddleware], getIsAdmin)
}