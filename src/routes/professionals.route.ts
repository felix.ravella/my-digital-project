import bodyParser from 'body-parser';

import { Application } from 'express';
import { deleteProfessional, getProfessional, getProfessionals, patchProfessional, postProfessional } from '../controllers/professionals.controller';
import { authenticationMiddleware } from '../middlewares/authenticationMiddleware';
import { isAdminMiddleware } from '../middlewares/isAdminMiddleware';

export function professionalsRoute(app: Application) {

  app.get('/professionals', getProfessionals);
  app.get('/professionals/:id', getProfessional);

  app.post('/professionals', [bodyParser.json(), authenticationMiddleware, isAdminMiddleware], postProfessional);
  app.patch('/professionals/:id', [bodyParser.json(), authenticationMiddleware, isAdminMiddleware], patchProfessional);
  app.delete('/professionals/:id', [bodyParser.json(), authenticationMiddleware, isAdminMiddleware], deleteProfessional);

}