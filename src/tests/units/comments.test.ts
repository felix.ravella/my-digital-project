import { createComment, destroyComment, findCommentsByArticleId } from '../../repositories/comments.repository';

const correctPayload = {
  content: "This is a content",
  articleId: 1,
  accountId: 1,
}

const incompletePayload = { 
  // content is missing
  articleId: 1,
  accountId: 1,
}

let createdCommentId = 0;

describe('Interracts with comments', () => {
  test('Prevents incomplete user creation', async () => {
    await expect(createComment(incompletePayload))
    .rejects
    .toThrow('Invalid `prisma.comment.create()`')
  });

  test('Creates new comment', async () => {
    const result =  await createComment(correctPayload);
    expect(typeof result).toBe("object");
    createdCommentId = result.id;
  });

  test('Retrieves comments by articleId', async() => {
    const results =  await findCommentsByArticleId(1);
    expect(results).toBeDefined;
    expect(typeof results).toBe("object");
    expect(results.length).toBeGreaterThan(0);
  })

  test('Deletes comment', async () => {
    const result =  await destroyComment(createdCommentId);
    expect(result).toBeDefined;
    expect(typeof result).toBe("object");
    expect(result.id).toBeDefined;
  })
})
