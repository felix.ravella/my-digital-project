
import { createAccount, findAccountByUsername, destroyAccount } from '../../repositories/accounts.repository';


const correctPayload = {
  username: "newUsername",
  password: "newPassword",
  roleId: 3,
  avatarAnimal: "fox",
  avatarColor: "red"
}

const incompletePayload = { 
  username: "newUsername",
  password: "newPassword",
  // roldeId is missing
  birthdate: "1989-11-03T12:00:00.000Z",
  lastName: "TestLast"
}

let createdAccountId = 0;

describe('Interracts with account and profile table',() => {
  test('Prevents incomplete user creation', async () => {
    await expect(createAccount(incompletePayload))
    .rejects
    .toThrow('Invalid `prisma.account.create()`')
  });

  test('Creates new user', async () => {
    const result =  await createAccount(correctPayload);
    expect(typeof result).toBe("object");
  });
  
  test('Prevents duplicate username', async () => {
    await expect(createAccount(correctPayload))
    .rejects
    .toThrow('Unique constraint failed on the constraint: `userName`')
  });
  
  
  test('Retrieves user by username', async() => {
    const result =  await findAccountByUsername('newUsername');
    expect(result).toBeDefined;
    expect(typeof result).toBe("object");
    expect(result.id).toBeDefined;
    createdAccountId = result.id;
  })

  
  test('Deletes user by id', async() => {
    const result =  await destroyAccount(createdAccountId);
    expect(result).toBeDefined;
    expect(typeof result).toBe("object");
    expect(result.id).toBeDefined;

  })
})
  



