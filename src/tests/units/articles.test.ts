import { createArticle, destroyArticle, findArticleById, updateArticle } from '../../repositories/articles.repository';

const correctPayload = {
  title: "Article title",
  content: "This is an article content",
  image: "https://www.random-image-site.com/images/1",
  tags: [2]
}

const incompletePayload = { 
  //title is missing
  content: "This is an article content",
  image: "https://www.random-image-site.com/images/1",
  tags: [2]
}

const updatePayload = {
  content: "Modified content",
  tags: [1]
}

let createdArticleId = 0;

describe('Interracts with articles', () => {
  test('Prevents incomplete article creation', async () => {
    await expect(createArticle(incompletePayload))
    .rejects
    .toThrow('Invalid `prisma.article.create()`')
  });

  test('Creates new article', async () => {
    const result =  await createArticle(correctPayload);
    expect(typeof result).toBe("object");
    createdArticleId = result.id;
  });
  
  test('Retrieves article by id', async() => {
    const result = await findArticleById(createdArticleId);
    expect(result).toBeDefined;
    expect(result.title).toBe('Article title');
  })

  test('Updates article', async () => {
    const result = await updateArticle(createdArticleId, updatePayload);
    expect(result).toBeDefined;
    expect(result.content).toBe('Modified content');
    expect(result.tags).toStrictEqual([{id: 1, label: "Annonce"}]);
  })

  test('Deletes article', async () => {
    const result =  await destroyArticle(createdArticleId);
    expect(result).toBeDefined;
    expect(typeof result).toBe("object");
    expect(result.id).toBeDefined;
  })
  
})
