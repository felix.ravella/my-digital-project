import { findAllTags, findTagById } from "../../repositories/tags.repository";

describe('Interracts with tag table',() => {
  test('Retrieves tags list', async () => {
    const results =  await findAllTags()
    expect(results).toBeDefined;
    expect(results.length).toBeGreaterThan(0);
  });

  test('Retrieves tag by id', async () => {
    const results =  await findTagById(1)
    expect(results).toBeDefined;
    expect(results.label).toBe('Annonce');
  });

})