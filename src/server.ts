import cors from 'cors';

import { getRoot } from './routes/getRoot';

import { tagsRoutes } from './routes/tags.route';
import { authRoutes } from './routes/auth.route';
import { usersRoute } from './routes/users.route';
import { articlesRoutes } from './routes/articles.route';
import { commentsRoute } from './routes/comments.route';
import { professionalsRoute } from './routes/professionals.route';
import { testResultsRoute } from './routes/testResults.route';

const express = require('express')
const app = express()


const corsOptions = {
  origin: ['https://digiquest.herokuapp.com', 'http://localhost:8080'],
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));

app.options('*', (req: any, res: any) => {
  res.status(200).end();
})

getRoot(app);
authRoutes(app);
usersRoute(app);
articlesRoutes(app);
tagsRoutes(app);
commentsRoute(app);
professionalsRoute(app);
testResultsRoute(app);

/*
// error handling
export const server = app.use((error: any, req: any, res: any) => {
  console.log('err')
  console.error(error);
})
*/

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`\nServer is listening on port ${port}\n`)
})
