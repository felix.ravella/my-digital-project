import { Request, Response } from "express";
import { createProfessional, destroyProfessional, findAllProfessionals, findProfessionalById, updateProfessional } from '../repositories/professionals.repository';

// GET /professionals/
export async function getProfessionals(req: Request, res: Response) {
  try {
    const results = await findAllProfessionals();
    res.status(200).send(results);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// GET /professionals/:id
export async function getProfessional(req: Request, res: Response) {
  try {
    const professionalId = parseInt(req.params.id)
    const results = await findProfessionalById(professionalId);
    res.status(200).send(results);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// POST /professionals
export async function postProfessional(req: Request, res: Response) {
  try {
    let {lastName, firstName, description, address, city, phone, url, specialties} = req.body;
    if(!lastName || !firstName || !description || !address || !city || !phone || !url || !specialties){
      res.status(400).send('Incomplete post Professional request')
      return;
    }

    const professional = await createProfessional(req.body);  
    res.status(200).send(professional);
  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
}

// PATCH /professionals/:id
export async function patchProfessional(req: Request, res: Response) {
  try {
    const professionalId = parseInt(req.params.id);
    const result = await updateProfessional(professionalId, req.body);
    res.status(200).send(result);
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return;
  }
}

// DELETE /professionals/:id
export async function deleteProfessional(req: Request, res: Response) {
  try {
    const professionalId = parseInt(req.params.id);
    await destroyProfessional(professionalId);
    res.status(200).send('Deletion complete');
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}