import { Request, Response } from "express";
import { createArticle, destroyArticle, findAllArticles, findArticleById, updateArticle } from '../repositories/articles.repository';


// GET /articles
export async function getArticles(req: Request, res: Response) {
  try {
    const articles = await findAllArticles();

    res.status(200).send(articles);
    return
  } catch (error) {
    res.status(500).send(error)
    return
  }
}

// GET /articles/:id
export async function getArticle(req: Request, res: Response) {
  try {
    const id = parseInt(req.params.id)
    const article = await findArticleById(id);

    res.status(200).send(article);
    return
  } catch (error) {
    res.status(500).send(error)
    return
  }
}

// POST /articles
export async function postArticle(req: Request, res: Response) {
  try {
    let { title, content, image, tags } = req.body;
    if (!image) { image = null };
    if (!tags) { tags = [] };

    if (!title || !content || !tags) {
      res.status(400).send('Incomplete post Article request')
      return;
    }

    const article = await createArticle({ title, content, image, tags });
    res.status(200).send(article);
  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
};

// PATCH /articles
export async function patchArticle(req: Request, res: Response) {
  try {
    const result = await updateArticle(parseInt(req.params.id), req.body);
    res.status(200).send(result);
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return;
  }
}

// DELETE /articles
export async function deleteArticle(req: Request, res: Response) {
  try {
    const articleId = parseInt(req.params.id);
    await destroyArticle(articleId);
    res.status(200).send('Deletion complete');
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }

}