import { Request, Response } from "express";
import { findArticleById } from "../repositories/articles.repository";
import { createComment, destroyComment, findAllComments, findCommentById, findCommentsByArticleId, updateComment } from "../repositories/comments.repository";

// GET /comments
export async function getComments(req: Request, res: Response) {
  try {
    const comments = await findAllComments();
    res.status(200).send(comments);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// GET /comments/:id
export async function getComment(req: Request, res: Response) {
  try {
    const id = parseInt(req.params.id)
    const comment = await findCommentById(id);
    res.status(200).send(comment);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// GET /articles/:id/comments
export async function getArticleComments(req: Request, res: Response) {
  try {
    const articleId = parseInt(req.params.id)
    const comments = await findCommentsByArticleId(articleId);
    res.status(200).send(comments);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}


// POST /comments
export async function postComment(req: Request, res: Response) {
  try {
    let { articleId, accountId, content } = req.body;
    if (!articleId || !accountId || !content) {
      res.status(400).send('Incomplete post Comment request')
      return;
    }

    const article = await findArticleById(articleId);
    if (!article) {
      res.status(404).send(`Cannot find article with id ${articleId}`)
      return
    }

    const comment = await createComment({ articleId, accountId, content });
    res.status(200).send(comment);
  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
}

// PATCH /comments/:id
export async function patchComment(req: Request, res: Response) {
  try {
    const commentId = parseInt(req.params.id);
    const result = await updateComment(commentId, req.body);
    res.status(200).send("Update sucessful");
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return;
  }
}

// DELETE /comments/:id
export async function deleteComment(req: Request, res: Response) {
  try {
    const commentId = parseInt(req.params.id);
    await destroyComment(commentId);
    res.status(200).send('Deletion complete');
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}


