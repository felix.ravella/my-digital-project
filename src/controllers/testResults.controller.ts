import { Request, Response } from "express";
import { createTestResult, destroyTestResult, findPendingTestResultByProfileId, findTestResult, findTestResultsByProfileId, updateTestResult } from "../repositories/testResults.repository";

// GET /testResults/:id
export async function getTestResult(req: Request, res: Response) {
  try {
    const testResultId = parseInt(req.params.id)
    const testResult = await findTestResult(testResultId);
    res.status(200).send(testResult);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// GET /users/:id/testResults
export async function getProfileTestResults(req: Request, res: Response) {
  try {
    const profileId = parseInt(req.params.id)
    const testResults = await findTestResultsByProfileId(profileId);
    res.status(200).send(testResults);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// GET /users/:id/testResults/pending
export async function getProfilePendingTestResult(req: Request, res: Response) {
  try {
    const profileId = parseInt(req.params.id)
    const testResults = await findPendingTestResultByProfileId(profileId);
    res.status(200).send(testResults);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// POST /testResults
export async function postTestResult(req: Request, res: Response) {
  try {
    let { profileId, ...rest } = req.body;
    if(!profileId || !rest.data ){
      res.status(400).send('Incomplete post TestResult request')
      return;
    }

    const testResult = await createTestResult({profileId, ...rest});  
    res.status(200).send(testResult);
  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
}

// PATCH /testResults/:id
export async function patchTestResult(req: Request, res: Response) {
  try {
    const result = await updateTestResult(parseInt(req.params.id), req.body);
    res.status(200).send(result);
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return;
  }
}

// DELETE /testResults/:id
export async function deleteTestResult(req: Request, res: Response) {
  try {
    await destroyTestResult(parseInt(req.params.id));
    res.status(200).send('Deletion complete');
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}