import { Request, Response } from "express";
import { findAllTags, findTagById } from "../repositories/tags.repository";


// GET /tags
export async function getTags(req: Request, res: Response) {
  try {
    const tags = await findAllTags();

    res.status(200).send(tags);
    return
  } catch (error) {
    res.status(500).send(error)
    return
  }
}

// GET /tags/:id
export async function getTag(req: Request, res: Response) {
  try {
    const id = parseInt(req.params.id)
    const tag = await findTagById(id);

    res.status(200).send(tag);
    return
  } catch (error) {
    res.status(500).send(error)
    return
  }
}