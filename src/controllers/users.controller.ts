const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

import { Request, Response } from "express";
import { createAccount, findAccountById, findAccountByUsername, updateAccount } from "../repositories/accounts.repository";
import { createProfile, destroyProfile, findProfileByEmail, updateProfile } from "../repositories/profiles.repository";

// GET /users/:id
export async function getUser(req: Request, res: Response) {
  try {
    const id = parseInt(req.params.id);
    const account = await findAccountById(id);
    if (!account) {
      res.status(404).send(`No account with id '${id} found.`)
      return
    }
    // exclude unecessary fields from returned data
    const { password, ...data } = account;
    res.status(200).send(data);
    return
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}

// POST /users
export async function postUser(req: Request, res: Response) {
  try {
    let { username, email, password, lastName, firstName, birthDate, } = req.body;
    if (!username || !email || !password || !lastName || !firstName || !birthDate) {
      res.status(400).send('Incomplete post User request')
      return;
    }
    let existingAccount = await findAccountByUsername(username);
    if (!!existingAccount) {
      res.status(409).send('Username is already taken')
      return;
    }

    let existingProfile = await findProfileByEmail(email);
    if (!!existingProfile) {
      res.status(409).send('Email is already taken')
      return;
    }

    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(password, salt);
    const account = await createAccount({ username, password: hashedPassword, avatarAnimal: "hummingbird", avatarColor: "blue", roleId: 3 })
    const profile = await createProfile({ id: account.id, email, lastName, firstName, birthDate });

    // Create token
    const token = jwt.sign(
      { userId: account.id },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: "30d",
      }
    );

    res.status(200).send({ token });

  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
}

// PATCH /users/:id
export async function patchUser(req: Request, res: Response) {
  try {
    const { profile, ...account } = req.body;
    const id = parseInt(req.params.id);

    if (Object.keys(account).length > 0) {
      await updateAccount(id, account);
    }
    if (profile && Object.keys(profile).length > 0) {
      await updateProfile(id, profile);
    }

    res.status(200).send('Account updated');
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return;
  }
}

// DELETE /users/:id
export async function deleteUser(req: Request, res: Response) {
  try {
    const id = parseInt(req.params.id)
    await destroyProfile(id);
    await updateAccount(id,{username: `Utilisateur inconnu-${id}`, disabled: true});
    res.status(200).send('Deletion complete');
    return;
  } catch (error) {
    console.log(error)
    res.status(500).send(error)
    return
  }
}