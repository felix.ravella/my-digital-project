const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
import { Request, Response } from "express";
import { findAccountByUsername } from "../repositories/accounts.repository";


// POST /auth/login
export async function postLogin(req: Request, res: Response) {
  try {
    let { username, password } = req.body;
    if (!username || !password) {
      res.status(400).send('Incomplete post Login request')
      return;
    }

    // check if account with username exists
    let existingAccount = await findAccountByUsername(username);
    if (!existingAccount) {
      res.status(401).send('Username / password mismatch')
      return;
    }

    const validPassword = await bcrypt.compare(password, existingAccount.password);
    if (!validPassword) {
      res.status(401).send('Username / password mismatch')
      return;
    }

    // Create token
    const token = jwt.sign(
      { userId: existingAccount.id },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: "30d",
      }
    );

    res.status(200).send({ token });
  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
}
// GET /auth/isLogged
export async function getIsLogged(req: Request, res: Response) {
  // it is assumed we went through the authentication middleware
  // at this point, we asserted token is genuine
  res.status(200).send("OK");
}

// GET /auth/isAdmin
export async function getIsAdmin(req: Request, res: Response) {
  // it is assumed we went through the authentication middleware and the isAdmin middleware
  // at this point, we asserted token is genuine and user has Admin role
  res.status(200).send("OK");
}