import { prisma } from "./prisma";

export async function findAllProfessionals() {
  const results = await prisma.professional.findMany({
    include: {
      specialties: true
    }
  })
  return results;
}

export async function findProfessionalById(id: number){
  const result = await prisma.professional.findUnique({
    where: {id},
    include: {
      specialties: true
    }
  });
  return result;
}

export async function createProfessional(payload: any){
  // aggreate all specialty ids
  let {specialties, ...professionalData} = payload;
  specialties = specialties.reduce((stack: Array<Record<string,Number>>, current: Number) => {
    return [...stack, {id: current}]
  }, [])

  // create new professional and generate connection to aggregated specialties
  const result = await prisma.professional.create({
    data: {
      ...professionalData,
      specialties: {
        connect: specialties
      }
    },
    include: {
      specialties: true
    }
  });

  return result;
}

export async function updateProfessional(id: number, payload: any){
  // aggreate all specialty ids
  let {specialties, ...professionalData} = payload;
  specialties = specialties.reduce((stack: Array<Record<string,Number>>, current: Number) => {
    return [...stack, {id: current}]
  }, [])
  
  // update professional, remove previous tag connections, and generate new connection to aggregated specialties
  const result = await prisma.professional.update({
    where: {id},
    data: {
      ...professionalData,
      specialties: {
        set: [],
        connect: specialties
      }
    },
    include: {
      specialties: true
    }
  });

  return result;
}

export async function destroyProfessional(id: any){
  return await prisma.professional.delete({where: {id}});
}
