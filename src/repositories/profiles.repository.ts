import { prisma } from "./prisma";

export async function findAllProfiles() {
  const results = await prisma.profile.findMany()
  return results;
}

export async function findProfileById(id: number){
  const result = await prisma.profile.findUnique({
    where: {id}
  });
  return result;
}

export async function findProfileByEmail(email: string){
  const result = await prisma.profile.findUnique({
    where: {email}
  });
  return result;
}

export async function createProfile(payload: any){
  const result = await prisma.profile.create({
    data: payload
  });
  return result;
}

export async function updateProfile(id: number, payload: any){
  const result = await prisma.profile.update({
    where: {id},
    data: payload
  });
  return result;
}

export async function destroyProfile(id: any){
  return await prisma.profile.delete({where: {id}});
}
