import { prisma } from "./prisma";

export async function findAllComments() {
  const results = await prisma.comment.findMany()
  return results;
}

export async function findCommentById(commentId: number){
  const result = await prisma.comment.findUnique({
    where: {id: commentId},
    include: {
      // retrieve author infos
      account: {
        select: {
          username: true,
          avatarAnimal: true,
          avatarColor: true,
        }
      }
    }
  });
  return result;
}

export async function findCommentsByAccountId(accountId: number){
  const results = await prisma.comment.findMany({
    where:{ accountId },
    include: {
      // retrieve author infos
      account: {
        select: {
          username: true,
          avatarAnimal: true,
          avatarColor: true,
        }
      }
    }
  });
  return results;
}

export async function findCommentsByArticleId(articleId: number){
  const results = await prisma.comment.findMany({
    where: {articleId},
    include: {
      // retrieve author infos
      account: {
        select: {
          username: true,
          avatarAnimal: true,
          avatarColor: true,
        }
      }
    }
  });
  return results;
}

export async function createComment(payload: any){
  const result = await prisma.comment.create({
    data: payload
  });
  return result;
}

export async function updateComment(commentId: number, payload: any){
  const result = await prisma.comment.update({
    where: {id: commentId},
    data: payload
  });
  return result;
}

export async function destroyComment(commentId: number){
  return await prisma.comment.delete({
    where: {id: commentId},
  });
}
