import { prisma } from "./prisma";

export async function findAllArticles() {
  const results = prisma.article.findMany({
    include: {
      tags: true
    }
  });
  return results;
}

export async function findArticleById(id: number) {
  const result = await prisma.article.findUnique({
    where: { id },
    include: {
      tags: true
    }
  });
  return result;
}

export async function createArticle(payload: any) {
  // aggregate all tag ids
  let { tags, ...articleData } = payload;
  tags = tags.reduce((stack: Array<Record<string, Number>>, current: Number) => {
    return [...stack, { id: current }]
  }, []);

  // create new article and generate connection to aggregated tags
  const result = await prisma.article.create({
    data: {
      ...articleData,
      tags: {
        connect: tags
      }
    },
    include: {
      tags: true
    }
  });

  return result;
}

export async function updateArticle(id: number, payload: any) {
  // aggreate all tag ids
  let { tags, ...articleData } = payload;
  tags = tags.reduce((stack: Array<Record<string, Number>>, current: Number) => {
    return [...stack, { id: current }]
  }, []);

  // update article, remove previous tag connections, and generate new connection to aggregated tags
  const result = await prisma.article.update({
    where: { id },
    data: {
      ...articleData,
      tags: {
        set: [],
        connect: tags
      }
    },
    include: {
      tags: true
    }
  });
  return result;
}

export async function destroyArticle(id: any) {
  return await prisma.article.delete({ where: { id } });
}
