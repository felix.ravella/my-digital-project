import { prisma } from "./prisma";

export async function findAllTestResults() {
  const results = await prisma.testResult.findMany()
  return results;
}

export async function findTestResult(testResultId: number){
  const result = await prisma.testResult.findUnique({
    where: { id: testResultId }
  });
  return result;
}

export async function findTestResultsByProfileId(profileId: number){
  const results = await prisma.testResult.findMany({
    where: {profileId}
  });
  return results
}

// returns the first incomplete testResult linked to given profileId
export async function findPendingTestResultByProfileId(profileId: number) {
  const result = await prisma.testResult.findFirst({
    where: {
      profileId,
      completedAt: null,
    }
  })
  return result;
}

export async function createTestResult(payload: any){
  const result = await prisma.testResult.create({
    data: payload
  });
  return result;
}

export async function updateTestResult(testResultId: number, payload: any){
  const result = await prisma.testResult.update({
    where: {id: testResultId},
    data: payload
  })
}

export async function destroyTestResult(id: number){
  return await prisma.testResult.delete({where: {id}});
}
