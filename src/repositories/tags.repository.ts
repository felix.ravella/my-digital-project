import { prisma } from "./prisma";

export async function findAllTags() {
  const result = await prisma.tag.findMany()
  return result;
}

export async function findTagById(id: number){
  const result = await prisma.tag.findUnique({
    where: {id},
  });
  return result;
}