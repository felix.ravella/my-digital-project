import { prisma } from "./prisma";

export async function findAllAccounts() {
  const result = await prisma.account.findMany()
  return result;
}


export async function findAccountById(id: number){
  const result = await prisma.account.findUnique({
    where: {id},
    include: {
      profile: {
        select: {
          lastName: true,
          firstName: true,
          email: true,
          birthDate: true
        }
      },
    }
  });
  return result;  
}

export async function findAccountByUsername(username: string){
  const result = await prisma.account.findUnique({
    where: {username}
  });
  return result;
}

export async function createAccount(payload: any){
  const result = await prisma.account.create({
    data: payload
  });
  return result;
}

export async function updateAccount(id: number, payload: any){
  const result = await prisma.account.update({
    where: {id},
    data: payload
  });
  return result;
}

export async function destroyAccount(id: any){
  return await prisma.account.delete({where: {id}});
}
