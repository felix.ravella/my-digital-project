import { NextFunction, Request, Response } from "express";
const jwt = require('jsonwebtoken');


// this middleware ensures that jwt exists in authorization headers, has not been forged, and has not expired

export function authenticationMiddleware(req: Request, res: Response, next: NextFunction){
  if (!req.headers.authorization) {
    res.status(401).send('Missing token!');
    return;
  }
  
  const token = req.headers.authorization.split(' ')[1];
  if (!token) {
    res.status(401).send('Missing token!')
    return;
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
  } catch (err) {
    res.status(403).send('Invalid token, please login again.');
    return;
  }

  next()
}
