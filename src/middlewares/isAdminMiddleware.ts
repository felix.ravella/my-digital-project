import { NextFunction, Request, Response } from "express";
import { findAccountById } from "../repositories/accounts.repository";
const jwt = require('jsonwebtoken');


// this middleware ensures that the account linked in the token has admin rights

export async function isAdminMiddleware(req: Request, res: Response, next: NextFunction){
  
  // it is assumed that the presence and the validity of the token has been verified 
  // before that with the authenticationMiddleware, so we just skip to the content verification

  const token = req.headers!.authorization!.split(' ')[1];

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);

    const account = await findAccountById(decoded.userId);
    if (!account) {
      res.status(403).send('Invalid token, please login again.');
      return; 
    }

    if (account.roleId !== 1) {
      res.status(403).send('You don\'t have sufficient rights to perform this operation');
      return; 
    }

  } catch (err) {
    res.status(403).send('Invalid token, please login again.');
    return;
  }


  next()
}
